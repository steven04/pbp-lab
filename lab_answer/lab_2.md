1. Apakah perbedaan antara JSON dan XML?
JSON merupakan format data turunan dari bahasa pemrograman Javascript, sehingga penulisannya juga sama seperti sintaks penulisan objek di Javascript. XML di sisi lain merupakan bahasa markup yang representasi datanya dituliskan di antara tag XML (mirip seperti tag pada HTML).

Perbedaan lainnya adalah format JSON lebih mendeskripsikan dirinya sendiri dan lebih mudah dibaca, sedangkan dokumen berformat XML akan lebih cocok apabila sebuah proyek membutuhkan markup dokumen dan informasi metadata.

2. Apakah perbedaan antara HTML dan XML?
HTML berfokus kepada penyajian data, sedangkan XML berfokus kepada transfer data. XML case sensitive, sedangkan HTML tidak. Tag XML memiliki nama tag yang tidak ditentukan sebelumnya, sedangkan HTML memiliki nama tag yang sudah ditentukan sebelumnya, seperti head, body, header, h1, p, dll.