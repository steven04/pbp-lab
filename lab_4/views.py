from django.shortcuts import render, redirect
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.


def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/lab-4/')
    else:
        form = NoteForm()
    return render(request, 'lab4_form.html', {'form': form})


def note_list(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)
