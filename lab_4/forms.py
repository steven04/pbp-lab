from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'

    to = forms.CharField(
        widget=forms.TextInput(attrs={'type':  'text', 'autocomplete': 'off'}),
    )

    From = forms.CharField(
        widget=forms.TextInput(attrs={'type':  'text', 'autocomplete': 'off'}),
    )

    title = forms.CharField(
        widget=forms.TextInput(attrs={'type':  'text', 'autocomplete': 'off'}),
    )

    message = forms.CharField(
        widget=forms.TextInput(attrs={'type':  'text', 'autocomplete': 'off'}),
    )
