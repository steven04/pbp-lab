from django import forms
from lab_1.models import Friend


class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'

    name = forms.CharField(
        widget=forms.TextInput(attrs={'type':  'text', 'autocomplete': 'off'}),
        label='Name'
    )

    npm = forms.CharField(
        widget=forms.TextInput(attrs={'type':  'text', 'autocomplete': 'off'}),
        label='NPM'
    )

    birth_date = forms.DateField(
        widget=forms.DateInput(attrs={'type': 'date', 'autocomplete': 'off'}),
        label='Date of Birth'
    )
