from lab_3.forms import FriendForm
from django.shortcuts import render, redirect
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)


@login_required(login_url='/admin/login/')
def add_friend(request):
    if request.method == "POST":
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/lab-3/')
    else:
        form = FriendForm()
    return render(request, 'lab3_form.html', {'form': form})
