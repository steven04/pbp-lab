import 'package:flutter/material.dart';
import '../widgets.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          'Selamat datang di dashboard.\nBerikut adalah data covid-19 terkini.',
          style: TextStyle(fontSize: 18),
        ),
        CardWidget("Terkonfirmasi", "421321", Colors.orange),
        CardWidget("Dalam Perawatan", "321321", Colors.blue),
        CardWidget("Sembuh", "101023", Colors.green),
        CardWidget("Meninggal", "3213912", Colors.red),
      ]),
    );
  }
}
