import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  CardWidget(this.title, this.jumlah, this.color);
  final title;
  final jumlah;
  final color;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(
          vertical: 8.0,
        ),
        padding: EdgeInsets.symmetric(
          vertical: 16.0,
          horizontal: 24.0,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(color: color),
            ),
            Padding(
              padding: EdgeInsets.all(4.0),
              child: Text(jumlah),
            )
          ],
        ));
  }
}
